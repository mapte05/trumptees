var Twitter = require('twitter');
var dotenv = require('dotenv').load();


var client = new Twitter({
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token_key: process.env.ACCESS_TOKEN_KEY,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET
});

var params = {screen_name: 'donaldtrumptees'};
client.get('statuses/user_timeline', params, function(error, tweets, response){
  if (!error) {
    for (var i=0; i<tweets.length; i++) {
      var tweet = tweets[i];
      console.log(tweet["text"]);
    }
  }
});